import 'package:flutter/material.dart';

void main() {
  runApp(Weather());
}

class Weather extends StatefulWidget {
  @override
  State<Weather> createState() => _Weather();
}

class _Weather extends State<Weather> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(32),height: 2000,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    "https://i.pinimg.com/736x/e7/39/22/e73922e2ccbaa06fb4f1532c6e34f68b.jpg"
                ),
                fit: BoxFit.cover
            ),

          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              Text('กรุงเทพฯ', style: const TextStyle(
                  color: Colors.white,
                  fontSize: 40,
                  fontWeight:  FontWeight.w200
              ),),
              Text('27°', textAlign: TextAlign.center,style: const TextStyle(
                  color: Colors.white,
                  fontSize: 80,
                  fontWeight:  FontWeight.w500
              ),),
              Text('เมฆเป็นบางส่วน', style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight:  FontWeight.w300
              ),),
              Text('สูงสุด: 31° ต่ำสุด: 21° ', style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight:  FontWeight.w300
              ),),

              const SizedBox(
                height: 58,
              ),
              Container(
                // alignment: Alignment.center,
                  margin: EdgeInsets.all(0.1),
                  height: 190,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    // color: Colors.blue.shade300,
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),

                    ],

                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 17,
                        left: 20,
                        child: Text('มีเมฆเป็นบางส่วนระหว่างเวลา 03:00-05:00', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),),
                      ),
                      Positioned(
                          child:  Divider(color: Colors.white60,
                            height: 100,
                            thickness: 0.9,
                            indent: 15,
                          )
                      ),

                      Positioned(
                        top: 60,
                        left: 20,
                        child: Text('ตอนนี้', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 100,
                        left: 24,
                        child:  Icon(Icons.wb_cloudy , color: Colors.white,),
                      ),
                      Positioned(
                        top: 145,
                        left: 27,
                        child: Text('27°', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 62,
                        left: 100,
                        child: Text('21', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 100,
                        left: 100,
                        child:  Icon(Icons.wb_cloudy , color: Colors.white,),
                      ),
                      Positioned(
                        top: 145,
                        left: 100,
                        child: Text('27°', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 62,
                        left: 163,
                        child: Text('22', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 100,
                        left: 163,
                        child:  Icon(Icons.wb_cloudy , color: Colors.white,),
                      ),
                      Positioned(
                        top: 145,
                        left: 163,
                        child: Text('26°', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 62,
                        left: 230,
                        child: Text('23', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 100,
                        left: 230,
                        child:  Icon(Icons.wb_cloudy , color: Colors.white,),
                      ),
                      Positioned(
                        top: 145,
                        left: 230,
                        child: Text('25°', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 62,
                        left: 293,
                        child: Text('00', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                      Positioned(
                        top: 100,
                        left: 293,
                        child:  Icon(Icons.wb_cloudy , color: Colors.white,),
                      ),
                      Positioned(
                        top: 145,
                        left: 293,
                        child: Text('25°', style:const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),),
                      ),
                    ],
                  )
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                // alignment: Alignment.center,
                margin: EdgeInsets.all(0.1),
                height: 250,
                width:double.infinity,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(20), //border corner radius
                  boxShadow:[
                    BoxShadow(
                      color: Colors.white.withOpacity(0), //color of shadow
                      spreadRadius: 5, //spread radius
                      blurRadius: 7, // blur radius
                      offset: Offset(0, 2), // changes position of shadow

                    ),

                  ],

                ),
                child: Stack(
                  children: [
                    Positioned(
                      top: 17,
                      left: 20,
                      child: Icon(Icons.calendar_month, color: Colors.white,),
                    ),
                    Positioned(
                      top: 17,
                      left: 20,
                      child: Text('         พยากรณ์อากาศ 10 วัน', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      ),),

                    ),
                    Positioned(
                        child:  Divider(color: Colors.white60,
                          height: 100,
                          thickness: 0.9,
                          indent: 15,
                        )
                    ),
                    Positioned(
                      top: 65,
                      left: 20,
                      child: Text('วันนี้', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 65,
                      left: 95,
                      child:  Icon(Icons.cloud , color: Colors.white,),
                    ),
                    Positioned(
                      top: 65,
                      left: 160,
                      child: Text('21°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 58,
                      left: 205,
                      child: Text('▂▂▂▂▂', style:const TextStyle(
                        color: Colors.amber,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),Positioned(
                      top: 65,
                      left: 290,
                      child: Text('31°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 105,
                      left: 20,
                      child: Text('ส.', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 105,
                      left: 95,
                      child:  Icon(Icons.cloud , color: Colors.white,),
                    ),
                    Positioned(
                      top: 105,
                      left: 160,
                      child: Text('21°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 98,
                      left: 205,
                      child: Text('▂▂▂▂▂', style:const TextStyle(
                        color: Colors.orange,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),Positioned(
                      top: 105,
                      left: 290,
                      child: Text('28°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 150,
                      left: 20,
                      child: Text('อา.', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 150,
                      left: 95,
                      child:  Icon(Icons.cloud , color: Colors.white,),
                    ),
                    Positioned(
                      top: 150,
                      left: 160,
                      child: Text('21°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 142,
                      left: 205,
                      child: Text('▂▂▂▂▂', style:const TextStyle(
                        color: Colors.amberAccent,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),Positioned(
                      top: 150,
                      left: 290,
                      child: Text('30°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 195,
                      left: 20,
                      child: Text('จ.', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 195,
                      left: 95,
                      child:  Icon(Icons.sunny , color: Colors.amberAccent,),
                    ),
                    Positioned(
                      top: 195,
                      left: 160,
                      child: Text('21°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),
                    Positioned(
                      top: 187,
                      left: 205,
                      child: Text('▂▂▂▂▂', style:const TextStyle(
                        color: Colors.amber,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),Positioned(
                      top: 195,
                      left: 290,
                      child: Text('32°', style:const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),),

                    ),


                  ],

                ),
              ),
            ],

          ),

        ),


      ),

    );
  }
}









